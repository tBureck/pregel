﻿using System;
using System.Runtime.Serialization;

namespace TBureck.PregelOGM
{

    public class OgmException : Exception
    {
        
        public OgmException() { }
        protected OgmException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public OgmException(string message) : base(message) { }
        public OgmException(string message, Exception innerException) : base(message, innerException) { }
    }
}