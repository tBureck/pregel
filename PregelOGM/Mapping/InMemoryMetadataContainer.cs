﻿using System;
using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping
{

    public class InMemoryMetadataContainer : IMetadataContainer
    {

        public Dictionary<Type, IMetadata> TypeMappings { get; set; }

        public InMemoryMetadataContainer()
        {
            this.TypeMappings = new Dictionary<Type, IMetadata>();
        }

        public void Merge(IMetadataContainer container)
        {
            throw new NotImplementedException();
        }
    }
}
