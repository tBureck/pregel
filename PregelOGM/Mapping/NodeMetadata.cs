﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping
{

    public class NodeMetadata : INodeMetadata
    {

        public string ClassName { get; set; }
        
        public string IdField { get; set; }

        public string Label { get; set; }
        public Dictionary<string, Dictionary<string, string>> Properties { get; set; }

        public NodeMetadata()
        {
            this.Properties = new Dictionary<string, Dictionary<string, string>>();
        }
    }
}
