﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace TBureck.PregelOGM.Mapping
{
    
    public class DefaultTypeResolver : ITypeResolver
    {

        private static string Pattern => "([a-z0-9.]+)(<([a-z0-9.]+,? ?)+>|\\[\\])?";

        private static Match MatchPattern(string typeName)
        {
            Regex regex = new Regex(Pattern, RegexOptions.IgnoreCase);
            Match match = regex.Match(typeName);

            if (match.Success == false) {
                throw new ArgumentException("Could not determine type from type name: type name is invalid.", "typeName");
            }
            
            return match;
        }

        public string GetMainType(string typeName)
        {
            Match match = MatchPattern(typeName);
            string className = match.Groups[1].Value;

            return className.Contains(".") == false ? this.ResolveShortType(className) : className;
        }

        public bool IsGeneric(string typeName)
        {
            Match match = MatchPattern(typeName);

            return match.Groups[2].Value != "";
        }

        public List<string> GetTypeParameters(string typeName)
        {
            Match match = MatchPattern(typeName);
            List<string> typeParameters = new List<string>();

            if (match.Groups[2].Value == "") {
                throw new ArgumentException("Given type name is not a generic type.", "typeName");
            }

            if (match.Groups[2].Value.StartsWith("[")) {
                typeParameters.Add(match.Groups[1].Value);
            }
            
            if (match.Groups[2].Value.StartsWith("<")) {
                typeParameters.AddRange(match.Groups[3].Value.Split(','));
            }

            return typeParameters;
        }

        public Type ResolveType(string typeName)
        {
            Match match = MatchPattern(typeName);

            bool isGeneric = false;
            string className = match.Groups[1].Value;

            // If no "." is present, a short type has been used. Resolve that to the real class:
            if (className.Contains(".") == false) {
                className = this.ResolveShortType(className);
            }
            
            List<string> parametersClassNames = new List<string>();

            if (match.Groups[2].Value != "") {
                isGeneric = true; 

                if (match.Groups[2].Value.StartsWith("[")) {
                    parametersClassNames.Add(className);
                    className = "System.Collections.Generic.List";
                }

                if (match.Groups[2].Value.StartsWith("<")) {
                    parametersClassNames.AddRange(match.Groups[3].Value.Split(','));
                }
            }

            if (isGeneric) {
                className += string.Format("`{0}[{1}]", parametersClassNames.Count, string.Join(",", parametersClassNames));
            }
            
            Type mainType = null;
            
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                if ((mainType = assembly.GetType(className)) != null && (isGeneric == false || mainType.IsGenericType)) {
                    break;
                }
            }
            
            return mainType;
        }

        protected string ResolveShortType(string shortName)
        {
            shortName = shortName.ToLower();
            
            switch (shortName) {
                case "string": 
                    return "System.String";
                case "int":
                case "int32":
                case "integer":
                    return "System.Int32";
                case "long":
                case "int64":
                    return "System.Int64";
            }

            return "";
        }
    }
}