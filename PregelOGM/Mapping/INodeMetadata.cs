﻿namespace TBureck.PregelOGM.Mapping
{

    public interface INodeMetadata : IMetadata
    {

        string Label { get; set; }
    }
}
