﻿using System;
using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping
{

    public interface IMetadataContainer
    {

        Dictionary<Type, IMetadata> TypeMappings { get; set; }

        /// <summary>
        /// In order to follow the "use only one metadata container per process" rule of thumb, this method
        /// allows to merge the given container into this container so that the other container can be safely
        /// disposed without loss of data.
        /// </summary>
        /// <param name="container">the container to merge into this container</param>
        void Merge(IMetadataContainer container);
    }
}
