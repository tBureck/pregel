﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping.Reader
{

    public interface IMetadataReader
    {

        /// <summary>
        /// This method will read metadata from a single source.
        /// </summary>
        /// <param name="source">The string representation of the source.</param>
        /// <returns>A new IMetadataContainer containing the type mappings from the given source.</returns>
        IMetadataContainer Read(string source);

        /// <summary>
        /// This method will read metadata from a single source and put the resulting mappings into the given IMetadataContainer.
        /// </summary>
        /// <param name="source">The string representation of the source.</param>
        /// <param name="container">The IMetadataContainer to put the type mappings into.</param>
        void Read(string source, IMetadataContainer container);

        /// <summary>
        /// Set custom configuration for this metadata reader. See the specific metadata reader's documentation for supported values.
        /// </summary>
        /// <param name="configuration">The configuration values to set.</param>
        void SetConfiguration(IDictionary<string, object> configuration);
    }
}
