﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TBureck.PregelOGM.Json.Converter;

namespace TBureck.PregelOGM.Mapping.Reader
{

    public class JsonMetadataReader : IMetadataReader
    {

        protected readonly string FileExtension = ".json";
        protected List<string> Paths;

        public IMetadataContainer Read(string source)
        {
            IMetadataContainer container = new InMemoryMetadataContainer();

            try {
                using (StreamReader sr = new StreamReader(this.GetFullSourcePath(source))) {
                    this.ReadMetadataFromStream(sr, container);
                }
            } catch (ArgumentException ae) {
                throw new OgmException($"No mapping information found for {source}. Did you forget to add a directory to the paths? Is the mapping file's name correct?", ae);
            }

            return container;
        }

        public void Read(string source, IMetadataContainer container)
        {
            try {
                using (StreamReader sr = new StreamReader(this.GetFullSourcePath(source))) {
                    this.ReadMetadataFromStream(sr, container);
                }
            } catch (ArgumentException ae) {
                throw new OgmException($"No mapping information found for {source}. Did you forget to add a directory to the paths? Is the mapping file's name correct?", ae);
            }
        }

        protected void ReadMetadataFromStream(StreamReader streamReader, IMetadataContainer container)
        {
            string contents = streamReader.ReadToEnd();
            
            JObject contentObject = (JObject) JsonConvert.DeserializeObject(contents);

            if (contentObject["EntityType"] == null) {
                // TODO Mapping does not specify entity type
                throw new OgmException("Mapping does not specify entity type.");
            }

            string mappingEntityType = contentObject["EntityType"].ToString();
            IMetadata metadata;
            
            if (mappingEntityType == EntityType.Node.ToString()) {
                metadata = JsonConvert.DeserializeObject<NodeMetadata>(contents);
            } else if (mappingEntityType == EntityType.Edge.ToString()) {
                metadata = JsonConvert.DeserializeObject<RelationMetadata>(contents, new NodeRelationMetadataConverter());
            } else {
                throw new OgmException(string.Format("Mapping specifies unknown entity type {0}", mappingEntityType));
            }

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                Type type;

                if ((type = assembly.GetType(metadata.ClassName)) == null) {
                    continue;
                }

                // TODO If no IdField has been specified in mapping, but there is an Id field on the class, assume it is the IdField?
                if (contentObject["IdField"] == null && type.GetProperty("Id") != null) {
                    metadata.IdField = "Id";
                }
                    
                container.TypeMappings[type] = metadata;
            }
        }

        public void SetConfiguration(IDictionary<string, object> configuration)
        {
            List<string> paths = new List<string> {
                "./"
            };

            if (configuration.ContainsKey("Paths") && configuration["Paths"] is List<string>) {
                paths = (List<string>) configuration["Paths"];
            }

            this.Paths = paths;
        }

        protected string GetFullSourcePath(string sourceName)
        {
            foreach (string path in this.Paths) {
                string fullPath = path + sourceName + this.FileExtension;

                if (File.Exists(fullPath)) {
                    return fullPath;
                }
            }

            return string.Empty;
        }
    }
}
