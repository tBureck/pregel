﻿namespace TBureck.PregelOGM.Mapping
{

    public interface IRelationMetadata : IMetadata
    {
        
        string Type { get; set; }
        INodeRelationMetadata From { get; set; }
        INodeRelationMetadata To { get; set; }
    }
}