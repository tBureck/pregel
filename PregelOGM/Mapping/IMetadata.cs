﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping
{

    public interface IMetadata
    {

        string ClassName { get; set; }
        string IdField { get; set; }
        Dictionary<string, Dictionary<string, string>> Properties { get; set; }
    }
}