﻿using System;
using System.Collections.Generic;

namespace TBureck.PregelOGM.Mapping 
{
    
    public interface ITypeResolver
    {

        string GetMainType(string typeName);
        bool IsGeneric(string typeName);
        List<string> GetTypeParameters(string typeName);
        
        Type ResolveType(string typeName);
    }
}