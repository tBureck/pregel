﻿namespace TBureck.PregelOGM.Mapping
{
    
    public interface INodeRelationMetadata
    {
        
        string TargetType { get; set; }
        string Field { get; set; }
    }
}