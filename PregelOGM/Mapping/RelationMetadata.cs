﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TBureck.PregelOGM.Mapping
{
    
    [JsonObject]
    public class RelationMetadata : IRelationMetadata
    {
        
        [JsonProperty]
        public string ClassName { get; set; }
        [JsonProperty]
        public string IdField { get; set; }
        [JsonProperty]
        public Dictionary<string, Dictionary<string, string>> Properties { get; set; }
        [JsonProperty]
        public string Type { get; set; }
        
        public INodeRelationMetadata From { get; set; }
        public INodeRelationMetadata To { get; set; }
    }
}