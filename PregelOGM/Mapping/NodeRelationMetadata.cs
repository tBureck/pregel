﻿namespace TBureck.PregelOGM.Mapping
{
    
    public class NodeRelationMetadata : INodeRelationMetadata 
    {
        
        public string TargetType { get; set; }
        public string Field { get; set; }
    }
}