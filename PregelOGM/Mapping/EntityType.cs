﻿namespace TBureck.PregelOGM.Mapping
{
    
    public enum EntityType
    {

        Node,
        Edge
    }
}