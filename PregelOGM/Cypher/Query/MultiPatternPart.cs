﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class MultiPatternPart : IPatternPart
    {

        public IPatternPart[] Parts { get; }

        public MultiPatternPart(IPatternPart[] parts)
        {
            this.Parts = parts;
        }
    }
}