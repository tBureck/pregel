﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class PatternClause : IQueryPart
    {
        
        public Pattern Pattern { get; }

        public PatternClause() : this(null) { }

        public PatternClause(Pattern pattern)
        {
            this.Pattern = pattern;
        }
    }
}