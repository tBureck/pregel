﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Create : PatternClause
    {

        public Create(Pattern pattern) : base(pattern) { }
    }
}