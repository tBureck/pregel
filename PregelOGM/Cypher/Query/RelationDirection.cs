﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public enum RelationDirection
    {
        
        Any,
        Bidirectional,
        Inbound,
        Outbound
    }
}