﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Where : IQueryPart
    {

        public string Predicate { get; }
        
        public Where(string predicate)
        {
            this.Predicate = predicate;
        }
    }
}