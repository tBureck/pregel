﻿using System;

namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Relation : IPatternPart
    {
        
        public string Alias { get; }
        public RelationDirection Direction { get; }
        public Type Type { get; }

        public Relation(string alias = "", RelationDirection direction = RelationDirection.Any, Type type = null)
        {
            this.Alias = alias;
            this.Direction = direction;
            this.Type = type;
        }
    }
}