﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Return : IQueryPart
    {
        
        public string[] Identifiers { get; }

        public Return(string[] identifiers)
        {
            this.Identifiers = identifiers;
        }
    }
}