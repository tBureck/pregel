﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Match : PatternClause
    {
        public bool Optional { get; }

        public Match() : this(false, null) { }

        public Match(bool optional, Pattern pattern) : base(pattern)
        {
            this.Optional = optional;
        }
    }
}