﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Query : IQuery
    {
        
        protected IEntityManager EntityManager { get; }
        public Dictionary<string, dynamic> Parameters { get; set; }
        public string Cypher { get; set; }

        public Query(IEntityManager entityManager)
        {
            this.EntityManager = entityManager;
        }

        public T Execute<T>()
        {
            this.EntityManager.ExecuteQuery(this.Cypher, this.Parameters);

            return default(T);
        }
    }
}