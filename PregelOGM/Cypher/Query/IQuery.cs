﻿namespace TBureck.PregelOGM.Cypher.Query
{
    
    public interface IQuery
    {

        string Cypher { get; }
        T Execute<T>();
    }
}