﻿using System;
using System.Collections.Generic;

namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Node : IPatternPart
    {
        
        public string Alias { get; }
        public Type Label { get; }
        public Dictionary<string, dynamic> Properties { get; }

        public Node(string alias = "", Type label = null, Dictionary<string, dynamic> properties = null)
        {
            this.Alias = alias;
            this.Label = label;
            this.Properties = properties;
        }
    }
}