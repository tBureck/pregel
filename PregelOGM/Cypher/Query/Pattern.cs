﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Cypher.Query
{
    
    public class Pattern : IQueryPart
    {

        public List<IPatternPart> Parts { get; }

        public Pattern()
        {
            this.Parts = new List<IPatternPart>();
        }

        public Pattern Add(IPatternPart part)
        {
            this.Parts.Add(part);

            return this;
        }

        public Pattern Add(params IPatternPart[] parts)
        {
            this.Parts.Add(new MultiPatternPart(parts));
            
            return this;
        }
    }
}