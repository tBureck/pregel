﻿using System;
using System.Linq.Expressions;
using TBureck.PregelOGM.Cypher.Query;

namespace TBureck.PregelOGM.Cypher
{
    
    public interface IQueryBuilder
    {
        
        IEntityManager EntityManager { get; }

        IQueryBuilder Create(Pattern pattern);
        IQueryBuilder Match(Pattern pattern);
        IQueryBuilder Where(string predicate);
        IQueryBuilder Where<T0>(Expression<Func<T0, bool>> predicateFunc);
        IQueryBuilder Where<T0, T1>(Expression<Func<T0, T1, bool>> predicateFunc);
        IQueryBuilder Return(params string[] identifiers);

        IQuery GetQuery();
    }
}