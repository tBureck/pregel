﻿using System;
using System.Runtime.Serialization;

namespace TBureck.PregelOGM.Cypher
{
    
    public class CypherException : OgmException
    {
        
        public string Statement { get; set; }

        public CypherException() { }
        protected CypherException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public CypherException(string message) : base(message) { }
        public CypherException(string message, Exception innerException) : base(message, innerException) { }
    }
}