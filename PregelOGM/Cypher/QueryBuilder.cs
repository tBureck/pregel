﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using TBureck.PregelOGM.Cypher.Query;
using TBureck.PregelOGM.Mapping;
using Match = TBureck.PregelOGM.Cypher.Query.Match;

namespace TBureck.PregelOGM.Cypher
{
    
    public class QueryBuilder : IQueryBuilder
    {
        
        public IEntityManager EntityManager { get; }

        protected List<IQueryPart> QueryParts { get; set; }
        protected Dictionary<string, Type> Identifiers { get; set; }
        public Dictionary<string, dynamic> Parameters { get; }

        public QueryBuilder(IEntityManager entityManager)
        {
            this.EntityManager = entityManager;
            
            this.QueryParts = new List<IQueryPart>();
            this.Identifiers = new Dictionary<string, Type>();
            this.Parameters = new Dictionary<string, dynamic>();
        }

        public IQueryBuilder Create(Pattern pattern)
        {
            // TODO extract identifiers from pattern for vanity checks later on
            // compare: https://bitbucket.org/tBureck/pregel/issues/6/querybuilder-add-identifier-vanity-checks

            foreach (IPatternPart patternPart in pattern.Parts) {
                if (patternPart is Node node && node.Alias != "" && node.Label != null) {
                    this.Identifiers[node.Alias] = node.Label;
                } else if (patternPart is Relation relation && relation.Alias != "" && relation.Type != null) {
                    this.Identifiers[relation.Alias] = relation.Type;
                }
            }
            
            this.QueryParts.Add(new Create(pattern));

            return this;
        }

        public IQueryBuilder Match(Pattern pattern)
        {
            // TODO extract identifiers from pattern for vanity checks later on
            // compare: https://bitbucket.org/tBureck/pregel/issues/6/querybuilder-add-identifier-vanity-checks

            foreach (IPatternPart patternPart in pattern.Parts) {
                if (patternPart is MultiPatternPart multi) {
                    foreach (IPatternPart part in multi.Parts) {
                        this.AnalyzePatternPart(part);
                    }
                } else {
                    this.AnalyzePatternPart(patternPart);
                }
            }
            
            this.QueryParts.Add(new Match(false, pattern));

            return this;
        }

        protected void AnalyzePatternPart(IPatternPart part)
        {
            if (part is Node node && node.Alias != "" && node.Label != null) {
                this.Identifiers[node.Alias] = node.Label;
            } else if (part is Relation relation && relation.Alias != "" && relation.Type != null) {
                this.Identifiers[relation.Alias] = relation.Type;
            }
        }

        public IQueryBuilder Where(string predicate)
        {
            this.QueryParts.Add(new Where(predicate));
            
            return this;
        }

        public IQueryBuilder Where(LambdaExpression lambdaExpression)
        {
            string predicateString = lambdaExpression.Body.ToString();

            // Replace .NET operators with Cypher operators:
            predicateString = this.ReplaceOperators(predicateString);

            // TODO strip identifiers and map them to Neo4j namings
            Regex regex = new Regex(@"([a-z0-9_]+)\.([a-z0-9_]+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection matches = regex.Matches(predicateString);
            
            foreach (System.Text.RegularExpressions.Match match in matches) {
                if (this.Identifiers.ContainsKey(match.Groups[1].Value)) {
                    IMetadata metadata = this.EntityManager.GetMetadataFor(this.Identifiers[match.Groups[1].Value]);

                    if (match.Groups[2].Value == metadata.IdField) {
                        predicateString = predicateString.Replace(match.Groups[0].Value, $"ID({match.Groups[1].Value})");
                    } else if (metadata.Properties.ContainsKey(match.Groups[2].Value)) {
                        predicateString = predicateString.Replace(match.Groups[2].Value, metadata.Properties[match.Groups[2].Value]["name"]);
                    }
                }
            }

            this.QueryParts.Add(new Where(predicateString));

            return this;
        }

        public IQueryBuilder Where<T0>(Expression<Func<T0, bool>> predicateFunc)
        {
            return this.Where((LambdaExpression) predicateFunc);
        }

        public IQueryBuilder Where<T0, T1>(Expression<Func<T0, T1, bool>> predicateFunc)
        {
            return this.Where((LambdaExpression) predicateFunc);
        }

        protected string ReplaceOperators(string str)
        {
            // TODO replace other operators?
            return str
                .Replace("==", "=")
                .Replace("AndAlso", "AND");
        }

        public IQueryBuilder Return(params string[] identifiers)
        {
            // TODO check, whether all identifiers are present in the query
            // compare: https://bitbucket.org/tBureck/pregel/issues/6/querybuilder-add-identifier-vanity-checks
            
            this.QueryParts.Add(new Return(identifiers));
            
            return this;
        }

        public IQuery GetQuery()
        {
            return new Query.Query(this.EntityManager) { Cypher = this.BuildCypher(), Parameters = this.Parameters };
        }

        protected virtual string BuildCypher()
        {
            string cypher = "";

            foreach (IQueryPart queryPart in this.QueryParts) {
                switch (queryPart) {
                    case Create create:
                        cypher += this.BuildCreate(create);
                        break;
                    case Match match:
                        cypher += this.BuildMatch(match);
                        break;
                    case Return returnClause:
                        cypher += this.BuildReturn(returnClause);
                        break;
                    case Where whereClause:
                        cypher += this.BuildWhere(whereClause);
                        break;
                }

                // Add a space after each clause:
                cypher += " ";
            }

            return cypher.Trim();
        }

        protected virtual string BuildCreate(Create create)
        {
            return "CREATE " + this.BuildPattern(create.Pattern);
        }

        protected virtual string BuildMatch(Match match)
        {
            string cypher = "MATCH ";
            
            if (match.Optional) {
                cypher = "OPTIONAL " + cypher;
            }

            return cypher + this.BuildPattern(match.Pattern);
        }

        protected virtual string BuildPattern(Pattern pattern)
        {
            return pattern.Parts.Aggregate("", (current, patternPart) => {
                switch (patternPart) {
                    case MultiPatternPart multi:
                        return current + string.Join(", ", multi.Parts.Select(this.BuildPatternPart));
                    default:
                        return current + this.BuildPatternPart(patternPart);
                }
            });
        }

        protected virtual string BuildPatternPart(IPatternPart part)
        {
            switch (part) {
                case Node node:
                    return this.BuildNode(node);
                case Relation relation:
                    return this.BuildRelation(relation);
            }

            throw new CypherException(
                string.Format(
                    "Got object of class {0} in pattern. Only Node and Relation are supported.",
                    part.GetType().AssemblyQualifiedName
                )
            );
        }

        protected virtual string BuildNode(Node node)
        {
            string cypher = "(";

            if (node.Alias != "") {
                cypher += node.Alias;
            }

            INodeMetadata metadata = null;
            if (node.Label != null) {
                metadata = this.EntityManager.GetMetadataFor(node.Label) as INodeMetadata;

                if (metadata == null) {
                    throw new OgmException($"No class mapping found for class {node.Label.AssemblyQualifiedName}");
                }
            }

            if (node.Label != null) {
                cypher += ":" + metadata?.Label;
            }

            if (node.Properties != null) {
                cypher += " {";
                cypher += string.Join(", ", node.Properties.Select(x => {
                    string value = x.Value is string ? $"'{x.Value}'" : x.Value.ToString();
                    
                    return metadata.Properties[x.Key]["name"] + ": " + value;
                }));
                cypher += "}";
            }

            return cypher + ")";
        }

        protected virtual string BuildRelation(Relation relation)
        {
            string cypher = "";
            bool closeBracket = false;

            if (relation.Direction == RelationDirection.Inbound) {
                cypher += "<";
            }

            cypher += "-";

            if (relation.Alias != "") {
                closeBracket = true;
                cypher += "[" + relation.Alias;
            }

            if (relation.Type != null) {
                if (!(this.EntityManager.GetMetadataFor(relation.Type) is IRelationMetadata metadata)) {
                    throw new OgmException($"No class mapping found for class {relation.Type.AssemblyQualifiedName}");
                }

                if (!closeBracket) {
                    cypher += "[";
                    closeBracket = true;
                }
                
                cypher += ":" + metadata.Type;
            }

            if (closeBracket) {
                cypher += "]";
            }

            cypher += "-";

            if (relation.Direction == RelationDirection.Outbound) {
                cypher += ">";
            }

            return cypher;
        }

        protected virtual string BuildReturn(Return returnClause)
        {
            return "RETURN " + string.Join(",", returnClause.Identifiers);
        }

        protected virtual string BuildWhere(Where whereClause)
        {
            return "WHERE " + whereClause.Predicate;
        }
    }
}