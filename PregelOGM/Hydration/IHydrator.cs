﻿using Neo4j.Driver.V1;
using TBureck.PregelOGM.Mapping;

namespace TBureck.PregelOGM.Hydration
{
    
    public interface IHydrator
    {

        T Hydrate<T>(T obj, IEntity entity, IMetadata metadata) where T : class;
    }
}