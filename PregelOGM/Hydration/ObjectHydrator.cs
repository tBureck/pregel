﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Mapping;

namespace TBureck.PregelOGM.Hydration
{
    
    public class ObjectHydrator : IHydrator
    {

        protected ITypeResolver TypeResolver { get; }

        public ObjectHydrator()
        {
            this.TypeResolver = new DefaultTypeResolver();
        }

        public T Hydrate<T>(T obj, IEntity entity, IMetadata metadata) where T : class
        {
            Type type = typeof(T);
            
            foreach (KeyValuePair<string,Dictionary<string,string>> property in metadata.Properties) {
                // 1) Try to match field name to class (property.Key)
                MemberInfo[] memberInfos = type.FindMembers(
                    MemberTypes.Field | MemberTypes.Property,
                    BindingFlags.Instance | BindingFlags.Public,
                    (info, criteria) => info.Name == criteria.ToString(),
                    property.Key
                );

                if (memberInfos.Length < 1) {
                    // TODO Field/Property not found
                    Console.WriteLine("Could not resolve field or property {0}", property.Key);
                    continue;
                } 
                    
                if (memberInfos.Length > 1) {
                    // TODO Field/Property ambiguous
                    Console.WriteLine("Field or property {0} is ambiguous", property.Key);
                    continue;
                }
                    
                // 2) Try to find value key in node (Property.Value["name"])
                // 3) If it is not present, leave default value
                // 4) Otherwise, set value to obj.{propertyKey}
                if (entity.Properties.ContainsKey(property.Value["name"])) {
                    BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public;

                    if (memberInfos[0] is PropertyInfo) {
                        bindingFlags |= BindingFlags.SetProperty;
                    } else {
                        bindingFlags |= BindingFlags.SetField;
                    }

                    object propertyValue = entity.Properties[property.Value["name"]];
                    
                    string metadataTypeString = property.Value["type"];
                    Type metadataType = this.TypeResolver.ResolveType(metadataTypeString);
                    
                    // If the value's type - read from database - doesn't match the type specified in the
                    // metadata, try to convert it to this type:
                    if (propertyValue.GetType() != metadataType) {
                        if (propertyValue is List<object> list) {
                            Type conversionType = this.TypeResolver.GetTypeParameters(metadataTypeString).First().GetType();

                            propertyValue = this.ConvertList(list, conversionType);
                        } else {
                            propertyValue = Convert.ChangeType(propertyValue, metadataType);
                        }
                    }
                    
                    // Set the value from database on the object:
                    type.InvokeMember(
                        property.Key,
                        bindingFlags,
                        Type.DefaultBinder,
                        obj,
                        new[] { propertyValue }
                    );
                }
            }

            return obj;
        }

        private IList ConvertList(IEnumerable<object> source, Type targetType)
        {
            IList list = (IList) Activator.CreateInstance(typeof(List<>).MakeGenericType(targetType));

            foreach (object o in source) {
                list.Add(Convert.ChangeType(o, targetType));
            }

            return list;
        }
    }
}