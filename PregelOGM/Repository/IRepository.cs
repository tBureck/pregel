﻿namespace TBureck.PregelOGM.Repository
{

    public interface IRepository<T>
    {

        T Find(object id);
    }
}