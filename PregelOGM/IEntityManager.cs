﻿using System;
using System.Collections.Generic;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Hydration;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Proxy;

namespace TBureck.PregelOGM
{

    public interface IEntityManager
    {
        
        IDriver Driver { get; }
        IHydrator Hydrator { get; }
        UnitOfWork UnitOfWork { get; }
        
        IProxyGenerator ProxyGenerator { get; }

        IMetadata GetMetadata<T>() where T : class;
        IMetadata GetMetadataFor(Type type);

        List<IRecord> ExecuteQuery(string cypher, Dictionary<string, dynamic> parameters);
        T Find<T>(long id) where T : class;

        T CreateProxy<T>() where T : class;

        T Persist<T>(T entity) where T : class;
        void Remove(object entity);

        void Flush();
        void Flush(object entity);

        /// <summary>
        /// This method should return the "clean" type of a given type. This means that, if the given type is
        /// a proxy type (implements IEntityType), it should return its parent type (which is the proxied type).
        /// </summary>
        /// <param name="possibleProxyType">the possibly proxied type</param>
        /// <returns>the proxied type, if the given type is a proxy; the checked type otherwise</returns>
        Type GetProxiedType(Type possibleProxyType);
    }
}