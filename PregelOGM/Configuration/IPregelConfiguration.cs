﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Configuration
{

    public interface IPregelConfiguration
    {
        
        List<string> Paths { get; set; }
    }
}