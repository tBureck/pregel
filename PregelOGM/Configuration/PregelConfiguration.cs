﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Configuration
{

    public class PregelConfiguration : IPregelConfiguration
    {

        public string MetadataReaderClass { get; set; }

        public List<string> Paths { get; set; }
    }
}