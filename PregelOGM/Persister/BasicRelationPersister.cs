﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Cypher;
using TBureck.PregelOGM.Cypher.Query;
using TBureck.PregelOGM.Mapping;

namespace TBureck.PregelOGM.Persister
{
    
    public class BasicRelationPersister : IEntityPersister
    {
        
        private IDriver Driver { get; }
        
        public IEntityManager EntityManager { get; }
        public IMetadata Metadata { get; }
        public Type Type { get; }

        public BasicRelationPersister(IEntityManager entityManager, IRelationMetadata metadata, Type type)
        {
            this.EntityManager = entityManager;

            this.Driver = this.EntityManager.Driver;
            
            this.Metadata = metadata;
            this.Type = type;
        }

        public object LoadById(long id)
        {
            using (ISession session = this.Driver.Session()) {
                IStatementResult resultStmt = session.Run(this.GetFindSingleQuery(id));
                List<IRecord> result = resultStmt.ToList();

                if (result.Any() == false) {
                    return null;
                }
                
                if (result.Count > 1) {
                    CypherException ce = new CypherException { Statement = resultStmt.Summary.Statement.ToString() };
                    throw ce;
                }

                IRecord record = result.First();

                MethodInfo hydrationMethod = this.EntityManager.Hydrator.GetType()
                    .GetMethod("Hydrate")
                    .MakeGenericMethod(this.Type);

                MethodInfo proxyMethod = this.EntityManager.GetType()
                    .GetMethod("CreateProxy")
                    .MakeGenericMethod(this.Type);

                object hydratedObject = hydrationMethod.Invoke(this.EntityManager.Hydrator, new[] {
                    proxyMethod.Invoke(this.EntityManager, null), 
                    (IEntity) record["r"], 
                    this.Metadata
                });
                
                return hydratedObject;
            }
        }
        
        protected string GetFindSingleQuery(object id)
        {
            Pattern pattern = new Pattern();
            pattern
                .Add(new Node())
                .Add(new Relation("r", RelationDirection.Outbound))
                .Add(new Node());
            
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            qb
                .Match(pattern)
                .Where($"ID(r) = {id}")
                .Return("r");

            return qb.GetQuery().Cypher;
        }

        public long GetIdentifier(object entity)
        {
            PropertyInfo propertyInfo = entity.GetType().GetProperty(this.Metadata.IdField);
            if (propertyInfo.PropertyType != typeof(long)) {
                throw new ArgumentException("ID field of entity must be a long property.");
            }
            
            return (long) propertyInfo.GetValue(entity);
        }
    }
}