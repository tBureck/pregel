﻿using System;
using TBureck.PregelOGM.Mapping;

namespace TBureck.PregelOGM.Persister
{
    
    public interface IEntityPersister
    {

        IMetadata Metadata { get; }
        Type Type { get; }

        object LoadById(long id);
        
        long GetIdentifier(object entity);
    }
}