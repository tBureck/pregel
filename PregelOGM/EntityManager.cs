﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.DynamicProxy;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Configuration;
using TBureck.PregelOGM.Cypher;
using TBureck.PregelOGM.Cypher.Query;
using TBureck.PregelOGM.Hydration;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Mapping.Reader;
using TBureck.PregelOGM.Persister;
using TBureck.PregelOGM.Proxy;
using TBureck.PregelOGM.Proxy.Castle;
using IProxyGenerator = TBureck.PregelOGM.Proxy.IProxyGenerator;

namespace TBureck.PregelOGM
{

    public class EntityManager : IEntityManager
    {
        
        protected readonly PregelConfiguration Configuration;
        
        public IDriver Driver { get; }
        public IHydrator Hydrator { get; }
        
        protected IMetadataContainer MetadataContainer { get; }
        protected IMetadataReader MetadataReader { get; set; }
        
        public IProxyGenerator ProxyGenerator { get; }
        public UnitOfWork UnitOfWork { get; }
        
        protected Dictionary<object, dynamic> ManagedEntities { get; }
        protected List<dynamic> NewEntities { get; }

        public EntityManager(IDriver driver, PregelConfiguration configuration)
        {
            this.Configuration = configuration;
            this.Driver = driver;
            
            this.Hydrator = new ObjectHydrator();

            this.MetadataContainer = new InMemoryMetadataContainer();
            this.InitializeMetadataReader();
            
            this.ProxyGenerator = new DpProxyGenerator();
            this.UnitOfWork = new UnitOfWork(this);
            
            this.ManagedEntities = new Dictionary<object, dynamic>();
            this.NewEntities = new List<dynamic>();
        }

        protected void InitializeMetadataReader()
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                Type type;

                if ((type = assembly.GetType(this.Configuration.MetadataReaderClass)) == null) {
                    continue;
                }

                object reader = assembly.CreateInstance(type.FullName);

                if (reader is IMetadataReader == false) {
                    // TODO throw an exception instead
                    return;
                }

                this.MetadataReader = (IMetadataReader) reader;
                this.MetadataReader.SetConfiguration(new Dictionary<string, object> {
                    { "Paths", this.Configuration.Paths }
                });
                
                break;
            }
        }

        public IMetadata GetMetadata<T>() where T : class
        {
            return this.GetMetadataFor(typeof(T));
        }

        public IMetadata GetMetadataFor(Type type)
        {
            type = this.GetProxiedType(type);
            
            this.MetadataReader.Read(type.Name, this.MetadataContainer);

            return this.MetadataContainer.TypeMappings[type];
        }

        public List<IRecord> ExecuteQuery(string cypher, Dictionary<string, dynamic> parameters)
        {
            using (ISession session = this.Driver.Session()) {
                // TODO replace parameters in cypher
                
                IStatementResult resultStmt = session.Run(cypher);
                return resultStmt.ToList();
            }
        }

        public T Find<T>(long id) where T : class
        {
            Type proxiedType = this.GetProxiedType(typeof(T));
            
            // Check, whether there already is a managed version of this entity:
            if (this.UnitOfWork.TryGetById(id, proxiedType) is T entity) {
                return entity;
            }

            // Otherwise, load it from the database:
            IEntityPersister persister = this.UnitOfWork.GetPersister<T>();

//                    pattern.Add(new Node()).Add(new Relation("n", RelationDirection.Inbound, type)).Add(new Node());
//                    break;

            T result = persister.LoadById(id) as T;
            this.UnitOfWork.RegisterManaged(result);
            
            return result;
        }

        public T CreateProxy<T>() where T : class
        {
            return this.ProxyGenerator.Generate<T>();
        }

        public T Persist<T>(T entity) where T : class
        {
            T managedEntity = this.ProxyGenerator.Decorate(entity);
            ((IEntityProxy) managedEntity).EntityStatus = EntityStatus.New;
            
            this.NewEntities.Add(managedEntity);

            return managedEntity;
        }

        public void Remove(object entity)
        {
            throw new NotImplementedException();
        }

        public void Flush()
        {
            throw new NotImplementedException();
        }

        public void Flush(object entity)
        {
            throw new NotImplementedException();
        }

        public Type GetProxiedType(Type possibleProxyType)
        {
            return typeof(IEntityProxy).IsAssignableFrom(possibleProxyType) 
                ? possibleProxyType.BaseType 
                : possibleProxyType;
        }
    }
}