using System;
using System.Collections.Generic;
using System.Reflection;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Persister;

namespace TBureck.PregelOGM
{
    
    public class UnitOfWork
    {
        
        private IEntityManager EntityManager { get; }

        private readonly Dictionary<Type, Dictionary<long, object>> _identityMap;

        private readonly Dictionary<int, EntityStatus> _entityStatuses;
        private readonly List<object> _entityInsertions;

        private readonly Dictionary<Type, IEntityPersister> _entityPersisters;

        public UnitOfWork(IEntityManager entityManager)
        {
            this.EntityManager = entityManager;

            this._identityMap = new Dictionary<Type, Dictionary<long, object>>();
            
            this._entityStatuses = new Dictionary<int, EntityStatus>();
            this._entityInsertions = new List<object>();
            
            this._entityPersisters = new Dictionary<Type, IEntityPersister>();
        }

        public object TryGetById(long id, Type type)
        {
            return this._identityMap.ContainsKey(type) && this._identityMap[type].ContainsKey(id)
                ? this._identityMap[type][id]
                : null;
        }

        public void RegisterManaged(object entity)
        {
            // No entity given? We're done here:
            if (entity == null) {
                return;
            }
            
            Type proxiedType = this.EntityManager.GetProxiedType(entity.GetType());
            
            if (!this._identityMap.ContainsKey(proxiedType)) {
                this._identityMap[proxiedType] = new Dictionary<long, object>();
            }

            MethodInfo persisterMethod = this.GetType()
                .GetMethod("GetPersister")
                .MakeGenericMethod(proxiedType);

            long entityId = ((IEntityPersister) persisterMethod.Invoke(this, null)).GetIdentifier(entity);

            this._identityMap[proxiedType][entityId] = entity;
        }

        public IEntityPersister GetPersister<T>() where T : class
        {
            Type proxiedType = this.EntityManager.GetProxiedType(typeof(T));
            
            if (this._entityPersisters.ContainsKey(proxiedType)) {
                return this._entityPersisters[proxiedType];
            }
            
            IMetadata metadata = this.EntityManager.GetMetadataFor(proxiedType);
            IEntityPersister persister = null;

            switch (metadata) {
                case INodeMetadata nodeMetadata:
                    persister = new BasicNodePersister(this.EntityManager, nodeMetadata, proxiedType);
                    break;
                case IRelationMetadata relationMetadata:
                    persister = new BasicRelationPersister(this.EntityManager, relationMetadata, proxiedType);
                    break;
            }

            if (persister != null) {
                this._entityPersisters[proxiedType] = persister;
            }

            return persister;
        }

        public EntityStatus GetState(object entity, EntityStatus assume = EntityStatus.New)
        {
            int entityHash = this.GetEntityHash(entity);
            
            if (this._entityStatuses.ContainsKey(entityHash)) {
                return this._entityStatuses[entityHash];
            }
            
            this._entityStatuses[entityHash] = assume;
            
            return assume;
        }

        public void Persist(object entity)
        {
            EntityStatus entityStatus = this.GetState(entity);

            switch (entityStatus) {
                case EntityStatus.New:
                    this.PersistNew(entity);
                    break;
            }
        }

        private void PersistNew(object entity)
        {
            this._entityStatuses[entity.GetHashCode()] = EntityStatus.Managed;
            this._entityInsertions.Add(entity);
        }

        protected int GetEntityHash(object entity)
        {
            return entity.GetHashCode();
        }
    }
}