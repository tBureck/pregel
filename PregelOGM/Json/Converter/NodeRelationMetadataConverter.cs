﻿using System;
using Newtonsoft.Json.Converters;
using TBureck.PregelOGM.Mapping;

namespace TBureck.PregelOGM.Json.Converter
{
    
    public class NodeRelationMetadataConverter : CustomCreationConverter<INodeRelationMetadata>
    {
        
        public override INodeRelationMetadata Create(Type objectType)
        {
            return new NodeRelationMetadata();
        }
    }
}