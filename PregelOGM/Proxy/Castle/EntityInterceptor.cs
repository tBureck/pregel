﻿using System;
using Castle.DynamicProxy;

namespace TBureck.PregelOGM.Proxy.Castle
{
    
    public class EntityInterceptor : IInterceptor
    {

        public void Intercept(IInvocation invocation)
        {
            invocation.Proceed();

            if (!(invocation.InvocationTarget is IEntityProxy entityProxy)) {
                return;
            }

            if (
                entityProxy.EntityStatus != EntityStatus.New
                && invocation.Method.Name.StartsWith("set_", StringComparison.OrdinalIgnoreCase)
            ) {
                // TODO notify EntityManager, that this entity needs update
            }
        }
    }
}