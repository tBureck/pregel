﻿using System;
using System.Reflection;
using Castle.DynamicProxy;

namespace TBureck.PregelOGM.Proxy.Castle
{
    
    public class DefaultProxyGenerationHook : IProxyGenerationHook
    {
        
        public void MethodsInspected()
        {
            // Do nothing
        }

        public void NonProxyableMemberNotification(Type type, MemberInfo memberInfo)
        {
            MethodInfo methodInfo = memberInfo as MethodInfo;

            if (methodInfo != null && methodInfo.IsSpecialName && methodInfo.Name.StartsWith("set_")) {
                throw new InvalidOperationException(string.Format(
                    "Cannot proxy property setters of non-virtual properties. Please consider making the property {0} of type {1} virtual.",
                    methodInfo.Name.Substring("set_".Length),
                    type.FullName
                ));
            }
        }

        public bool ShouldInterceptMethod(Type type, MethodInfo methodInfo)
        {
            return true;
        }
    }
}