﻿using Castle.DynamicProxy;

namespace TBureck.PregelOGM.Proxy.Castle
{
    
    public class DpProxyGenerator : IProxyGenerator
    {

        protected readonly global::Castle.DynamicProxy.IProxyGenerator ProxyGenerator;

        public DpProxyGenerator()
        {
            this.ProxyGenerator = new ProxyGenerator();
        }
        
        public T Generate<T>() where T : class
        {
            return (T) this.ProxyGenerator.CreateClassProxy(
                typeof(T), 
                this.CreateProxyGenerationOptions(), 
                new EntityInterceptor()
            );
        }

        public T Decorate<T>(T target) where T : class
        {
            return this.ProxyGenerator.CreateClassProxyWithTarget(
                target, 
                this.CreateProxyGenerationOptions(),
                new EntityInterceptor()
            );
        }

        protected ProxyGenerationOptions CreateProxyGenerationOptions()
        {
            ProxyGenerationOptions options = new ProxyGenerationOptions(new DefaultProxyGenerationHook());
            options.AddMixinInstance(new DefaultEntityProxy());
            
            return options;
        }
    }
}