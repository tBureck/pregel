﻿namespace TBureck.PregelOGM.Proxy
{
    
    public interface IProxyGenerator
    {

        T Generate<T>() where T : class;
        T Decorate<T>(T target) where T : class;
    }
}