﻿namespace TBureck.PregelOGM.Proxy
{
    
    public interface IEntityProxy
    {
        
        IEntityManager EntityManager { get; set; }
        EntityStatus EntityStatus { get; set; }
    }
}