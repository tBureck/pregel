﻿namespace TBureck.PregelOGM.Proxy
{
    public class DefaultEntityProxy : IEntityProxy
    {
        
        public IEntityManager EntityManager { get; set; }
        public EntityStatus EntityStatus { get; set; }
    }
}