﻿using System;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Mapping.Reader;

namespace TBureck.Pregel.Example
{

    class Program
    {

        static void Main(string[] args) {
            IMetadataReader reader = new JsonMetadataReader();
            IMetadataContainer container = reader.Read("mappings/Movie.json");

            Console.WriteLine("Hi!'");
        }
    }
}
