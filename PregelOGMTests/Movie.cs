﻿namespace TBureck.PregelOGM.Tests
{

    public class Movie
    {

        public virtual long Id { get; set; }
        public virtual long Released { get; set; }
        public virtual string Tagline { get; set; }
        public virtual string Title { get; set; }
    }
}
