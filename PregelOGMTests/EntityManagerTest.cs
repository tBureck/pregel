﻿using System.Collections.Generic;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Configuration;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Proxy;
using Xunit;

namespace TBureck.PregelOGM.Tests
{

    public class EntityManagerTest
    {

        private IDriver _driver;
        private IEntityManager _entityManager;

        private IDriver Driver => this._driver ?? (this._driver = GraphDatabase.Driver("bolt://localhost:7687", AuthTokens.Basic("neo4j", "test")));
        private IEntityManager EntityManager => this._entityManager ?? (this._entityManager = new EntityManager(this.Driver, new PregelConfiguration {MetadataReaderClass = "TBureck.PregelOGM.Mapping.Reader.JsonMetadataReader", Paths = new List<string> {"./mappings/"}}));

        [Fact]
        public void Get_MovieMetadata()
        {
            IMetadata metadata = this.EntityManager.GetMetadata<Movie>();

            Assert.IsAssignableFrom<INodeMetadata>(metadata);
        }

        [Fact]
        public void Get_ActedInMetadata()
        {
            IMetadata metadata = this.EntityManager.GetMetadata<ActedIn>();

            Assert.IsAssignableFrom<IRelationMetadata>(metadata);
        }

        [Fact]
        public void Find_SingleMovie()
        {
            Movie movie = this.EntityManager.Find<Movie>(0);

            Assert.NotNull(movie);

            if (movie != null) {
                Assert.Equal("The Matrix", movie.Title);
            }
        }

        [Fact]
        public void Find_SingleActedIn()
        {
            ActedIn actedIn = this.EntityManager.Find<ActedIn>(1);
            
            Assert.NotNull(actedIn);

            if (actedIn != null) {
                Assert.Equal("Trinity", actedIn.Roles[0]);
            }
        }

        [Fact]
        public void Find_NoResult()
        {
            Movie movie = this.EntityManager.Find<Movie>(1);

            Assert.Null(movie);
        }

        [Fact]
        public void Find_SingleMovie_AlreadyManaged()
        {
            // First off, load a movie. If we now want to load it again, we should end up with the same
            // movie object.
            Movie initialMovie = this.EntityManager.Find<Movie>(0);
            Movie sameMovie = this.EntityManager.Find<Movie>(0);
            
            Assert.Same(initialMovie, sameMovie);
        }

        [Fact]
        public void Persist_MovieObject()
        {
            Movie movie = new Movie {Title = "The Croods"};
            Movie persistedMovie = this.EntityManager.Persist(movie);

            Assert.IsAssignableFrom<IEntityProxy>(persistedMovie);
            Assert.Equal(EntityStatus.New, ((IEntityProxy) persistedMovie).EntityStatus);
        }
    }
}