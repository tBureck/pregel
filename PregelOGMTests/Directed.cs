﻿namespace TBureck.PregelOGM.Tests
{
    
    public class Directed
    {

        public virtual long Id { get; set; }
        public virtual Person Person { get; set; }
        public virtual Movie Movie { get; set; }
    }
}