﻿using System.Collections.Generic;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Configuration;
using TBureck.PregelOGM.Cypher;
using TBureck.PregelOGM.Cypher.Query;
using Xunit;

namespace TBureck.PregelOGM.Tests.Cypher
{
    
    public class QueryBuilderTest
    {
        
        private IDriver _driver;
        private IEntityManager _entityManager;

        private IDriver Driver => this._driver ?? (this._driver = GraphDatabase.Driver("bolt://localhost:7687", AuthTokens.Basic("neo4j", "test")));
        private IEntityManager EntityManager => this._entityManager ?? (this._entityManager = new EntityManager(this.Driver, new PregelConfiguration {MetadataReaderClass = "TBureck.PregelOGM.Mapping.Reader.JsonMetadataReader", Paths = new List<string> {"./mappings/"}}));

        [Fact]
        public void MatchAllNodes()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(new Pattern().Add(new Node("n")))
                .Return("n")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (n) RETURN n", cypher);
        }

        [Fact]
        public void MatchAllMovies()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(new Pattern().Add(new Node("m", typeof(Movie))))
                .Return("m")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (m:Movie) RETURN m", cypher);
        }

        [Fact]
        public void MatchSingleMovieById()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(new Pattern().Add(new Node("m", typeof(Movie))))
                .Where<Movie>(m => m.Id == 0)
                .Return("m")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (m:Movie) WHERE (ID(m) = 0) RETURN m", cypher);
        }

        [Fact]
        public void MatchMoviesAfter2008()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(new Pattern().Add(new Node("m", typeof(Movie))))
                .Where<Movie>(m => m.Released >= 2008)
                .Return("m")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (m:Movie) WHERE (m.released >= 2008) RETURN m", cypher);
        }

        [Fact]
        public void MatchDirectors()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(
                    new Pattern()
                        .Add(new Node("p", typeof(Person)))
                        .Add(new Relation("dir", RelationDirection.Outbound, typeof(Directed)))
                        .Add(new Node("m"))
                )
                .Return("p")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (p:Person)-[dir:DIRECTED]->(m) RETURN p", cypher);
        }

        [Fact]
        public void CreateSingleNode()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Create(
                    new Pattern()
                        .Add(
                            new Node("a", typeof(Movie), new Dictionary<string, dynamic> {
                                {"Title", "The Croods"},
                                {"Tagline", "Gotta find a cave!"},
                                {"Released", 2013}
                            })
                        )
                )
                .Return("a")
                .GetQuery()
                .Cypher;
            
            Assert.Equal("CREATE (a:Movie {title: 'The Croods', tagline: 'Gotta find a cave!', released: 2013}) RETURN a", cypher);
        }

        [Fact]
        public void CreateSingleRelationship()
        {
            QueryBuilder qb = new QueryBuilder(this.EntityManager);
            string cypher = qb
                .Match(
                    new Pattern()
                        .Add(
                            new Node("a", typeof(Person)),
                            new Node("b", typeof(Movie))
                        )
                )
                .Where<Movie, Person>((a, b) => a.Id == 1 && b.Id == 2)
                .Create(
                    new Pattern()
                        .Add(new Node("a"))
                        .Add(new Relation(direction: RelationDirection.Outbound, type: typeof(Directed)))
                        .Add(new Node("b"))
                )
                .GetQuery()
                .Cypher;
            
            Assert.Equal("MATCH (a:Person), (b:Movie) WHERE ((ID(a) = 1) AND (ID(b) = 2)) CREATE (a)-[:DIRECTED]->(b)", cypher);
        }
    }
}