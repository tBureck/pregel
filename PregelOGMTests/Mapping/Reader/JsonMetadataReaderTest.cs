﻿using System;
using System.Collections.Generic;
using TBureck.PregelOGM.Mapping;
using TBureck.PregelOGM.Mapping.Reader;
using Xunit;

namespace TBureck.PregelOGM.Tests.Mapping.Reader
{
    
    public class JsonMetadataReaderTest
    {

        [Fact]
        public void Read_EasyMovieMetadata()
        {
            const string sourcename = "Movie";

            IMetadataReader reader = this.InitializeReader();
            IMetadataContainer container = reader.Read(sourcename);

            Type type = typeof(Movie);
            Assert.True(container.TypeMappings.ContainsKey(type));

            IMetadata metadata = container.TypeMappings[type];
            this.Assert_EasyMovieMetadataValidity(metadata);
        }

        [Fact]
        public void Read_EasyMovieMetadataIntoExistingContainer()
        {
            const string sourcename = "Movie";

            IMetadataContainer container = new InMemoryMetadataContainer();

            IMetadataReader reader = this.InitializeReader();
            reader.Read(sourcename, container);

            Type type = typeof(Movie);
            Assert.True(container.TypeMappings.ContainsKey(type));

            IMetadata metadata = container.TypeMappings[type];
            this.Assert_EasyMovieMetadataValidity(metadata);
        }

        protected void Assert_EasyMovieMetadataValidity(IMetadata metadata)
        {
            Assert.IsAssignableFrom<INodeMetadata>(metadata);

            if (metadata is INodeMetadata == false) {
                return;
            }

            INodeMetadata nodeMetadata = (INodeMetadata) metadata;
            
            Assert.True(nodeMetadata.Label == "Movie");
            
            Assert.True(nodeMetadata.Properties.ContainsKey("Released"));
            Assert.True(nodeMetadata.Properties.ContainsKey("Tagline"));
            Assert.True(nodeMetadata.Properties.ContainsKey("Title"));

            Dictionary<string, string> releasedProperty = nodeMetadata.Properties["Released"];
            Assert.True(releasedProperty.ContainsKey("name"));
            Assert.True(releasedProperty.ContainsKey("type"));

            Dictionary<string, string> taglineProperty = nodeMetadata.Properties["Tagline"];
            Assert.True(taglineProperty.ContainsKey("name"));
            Assert.True(taglineProperty.ContainsKey("type"));

            Dictionary<string, string> titleProperty = nodeMetadata.Properties["Title"];
            Assert.True(titleProperty.ContainsKey("name"));
            Assert.True(titleProperty.ContainsKey("type"));
        }

        [Fact]
        public void Read_EasyActedInMetadata()
        {
            const string sourcename = "ActedIn";

            IMetadataReader reader = this.InitializeReader();
            IMetadataContainer container = reader.Read(sourcename);

            Type type = typeof(ActedIn);
            Assert.True(container.TypeMappings.ContainsKey(type));

            IMetadata metadata = container.TypeMappings[type];
            this.Assert_EasyActedInMetadataValidity(metadata);
        }
        
        protected void Assert_EasyActedInMetadataValidity(IMetadata metadata)
        {
            Assert.IsAssignableFrom<IRelationMetadata>(metadata);

            if ((metadata is IRelationMetadata) == false) {
                return;
            }

            IRelationMetadata relationMetadata = (IRelationMetadata) metadata;
            
            Assert.True(relationMetadata.Type == "ACTED_IN");
            Assert.True(relationMetadata.Properties.ContainsKey("Roles"));

            Dictionary<string, string> rolesProperty = relationMetadata.Properties["Roles"];
            Assert.True(rolesProperty.ContainsKey("name"));
            Assert.True(rolesProperty.ContainsKey("type"));
            
            Assert.Null(relationMetadata.From);
            
            Assert.NotNull(relationMetadata.To);
            Assert.IsAssignableFrom<INodeRelationMetadata>(relationMetadata.To);
        }

        [Fact]
        public void Read_NonExistentMappingFile()
        {
            const string sourcename = "IDontExist";

            Assert.Throws<OgmException>(() => {
                IMetadataReader reader = this.InitializeReader();
                reader.Read(sourcename);
            });
        }

        protected JsonMetadataReader InitializeReader()
        {
            JsonMetadataReader reader = new JsonMetadataReader();
            reader.SetConfiguration(new Dictionary<string, object> {
                { "Paths", new List<string> { "./mappings/" } }
            });

            return reader;
        }
    }
}
