﻿using System.Collections.Generic;

namespace TBureck.PregelOGM.Tests
{
    
    public class ActedIn
    {
        
        public virtual long Id { get; set; }
        public virtual List<string> Roles { get; set; }
        public virtual Movie Movie { get; set; }
    }
}