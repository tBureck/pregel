using System.Collections.Generic;
using Neo4j.Driver.V1;
using TBureck.PregelOGM.Configuration;
using Xunit;

namespace TBureck.PregelOGM.Tests
{
    
    public class UnitOfWorkTest
    {
        
        private IDriver _driver;
        private IEntityManager _entityManager;
        private UnitOfWork _unitOfWork;

        private IDriver Driver => this._driver ?? (this._driver = GraphDatabase.Driver("bolt://localhost:7687", AuthTokens.Basic("neo4j", "test")));
        private IEntityManager EntityManager => this._entityManager ?? (this._entityManager = new EntityManager(this.Driver, new PregelConfiguration {MetadataReaderClass = "TBureck.PregelOGM.Mapping.Reader.JsonMetadataReader", Paths = new List<string> {"./mappings/"}}));

        private UnitOfWork UnitOfWork => this._unitOfWork ?? (this._unitOfWork = this.EntityManager.UnitOfWork);

        [Fact]
        public void TryGetMovieById()
        {
            // UnitOfWork is supposed to cache already loaded entities:
            this.EntityManager.Find<Movie>(0);
            
            Movie movie = this.UnitOfWork.TryGetById(0, typeof(Movie)) as Movie;

            Assert.NotNull(movie);

            if (movie != null) {
                Assert.Equal("The Matrix", movie.Title);
            }
        }
        
        [Fact]
        public void PersistNewSimpleNode()
        {
            Movie movie = new Movie {Title = "The Croods"};
            this.UnitOfWork.Persist(movie);

            Assert.Equal(EntityStatus.Managed, this.UnitOfWork.GetState(movie));
        }
    }
}