﻿namespace TBureck.PregelOGM.Tests
{
    
    public class NonVirtualMovie
    {
        
        public long Id { get; set; }
        public long Released { get; set; }
        public string Tagline { get; set; }
        public string Title { get; set; }
    }
}