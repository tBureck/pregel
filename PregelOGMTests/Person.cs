﻿namespace TBureck.PregelOGM.Tests
{
    
    public class Person
    {
        
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual long Born { get; set; }
    }
}