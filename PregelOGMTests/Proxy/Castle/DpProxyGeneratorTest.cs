﻿using System;
using Castle.DynamicProxy;
using TBureck.PregelOGM.Proxy;
using TBureck.PregelOGM.Proxy.Castle;
using Xunit;
using IProxyGenerator = TBureck.PregelOGM.Proxy.IProxyGenerator;

namespace TBureck.PregelOGM.Tests.Proxy.Castle
{
    
    public class DpProxyGeneratorTest
    {

        [Fact]
        public void Generate_MovieProxy()
        {
            IProxyGenerator proxyGenerator = new DpProxyGenerator();
            Movie movie = proxyGenerator.Generate<Movie>();
            
            Assert.IsAssignableFrom<Movie>(movie);
            
            // Assertion for proxy type; all generated proxies implement this interface:
            // http://kozmic.net/2009/01/27/castle-dynamic-proxy-tutorial-part-iv-breaking-hard-dependencies/
            Assert.IsAssignableFrom<IProxyTargetAccessor>(movie);

            Assert.IsAssignableFrom<IEntityProxy>(movie);
        }

        [Fact]
        public void Decorate_Movie()
        {
            IProxyGenerator proxyGenerator = new DpProxyGenerator();
            Movie movie = new Movie {Title = "The Croods"};

            Movie proxiedMovie = proxyGenerator.Decorate(movie);

            // Assertion for proxy type; all generated proxies implement this interface:
            // http://kozmic.net/2009/01/27/castle-dynamic-proxy-tutorial-part-iv-breaking-hard-dependencies/
            Assert.IsAssignableFrom<IProxyTargetAccessor>(proxiedMovie);
            
            // Data should not be lost! Is the title still "The Croods"?
            Assert.Equal("The Croods", proxiedMovie.Title);
        }

        [Fact]
        public void CheckNonVirtualDirtyState()
        {
            // Properties need to be virtual in order to be proxied; non-virtual property setters are not supported:
            Assert.Throws<InvalidOperationException>(() => {
                IProxyGenerator proxyGenerator = new DpProxyGenerator();
                NonVirtualMovie movie = proxyGenerator.Generate<NonVirtualMovie>();

                movie.Title = "This is a new title";
            });
        }
    }
}